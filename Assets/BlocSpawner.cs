using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cuadrado;
    public NBloques numeroBloques;

    void Start()
    {
        for (int i=0; i < numeroBloques.Value; i++)
        {
            GameObject newCuadrado = Instantiate(cuadrado);
            newCuadrado.transform.position = new Vector2(Random.Range(-9.6f,9.6f), Random.Range(0, 4));
            newCuadrado.name = "Bloc "+i;
        }
    }


}
