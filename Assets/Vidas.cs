using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Vidas : MonoBehaviour
{
    public StartBola bola;
    int vidas = 3;
    // Start is called before the first frame update
    void Start()
    {
        bola.onPerderVida += perdervida;
    }

    // Update is called once per frame
    void Update()
    {


    }
     void perdervida()
    {
        vidas--;
        this.gameObject.GetComponent<TextMeshProUGUI>().text =""+ vidas;
    }
}
