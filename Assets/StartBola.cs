using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Video;

public class StartBola : MonoBehaviour
{
    public GameObject pala;
    public delegate void vida();
    public event vida onPerderVida;
    bool delegado = false;
    int vidas = 3;
    Transform pos;
    // Start is called before the first frame update
    void Start()
    {
        this.transform.Rotate(0, 0, Random.Range(0, 360));
        this.GetComponent<Rigidbody2D>().velocity = transform.up * 5;
        pos=this.transform;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "trigger")
        {
            onPerderVida.Invoke();
            delegado = true;
            vidas--;
            if (vidas > 0)
            {
                this.transform.position = new Vector3(0, 0, 0);
                this.transform.Rotate(0, 0, Random.Range(0, 360));
                this.GetComponent<Rigidbody2D>().velocity = transform.up * 5;
            } else
            {
                Destroy(this.gameObject);
            }
        }
    }
}
