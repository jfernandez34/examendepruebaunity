using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoweUpScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -5);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag=="trigger")
        {
            Destroy(this.gameObject);
        }
    }
}
