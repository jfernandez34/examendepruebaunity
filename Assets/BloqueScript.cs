using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloqueScript : MonoBehaviour
{

    public GameObject powerup;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.tag!="bloque")
        {
            if (Random.Range(0,2)==0)
            {
                GameObject newPowerup = Instantiate(powerup);
                newPowerup.transform.position = this.transform.position;
                int randColor = Random.Range(0, 4);
                if (randColor==0)
                {
                    newPowerup.GetComponent<SpriteRenderer>().color=Color.red;
                }
                else if (randColor == 1)
                {
                    newPowerup.GetComponent<SpriteRenderer>().color = Color.blue;
                } 
                else if (randColor == 2)
                {
                    newPowerup.GetComponent<SpriteRenderer>().color = Color.yellow;
                }
                else
                {
                    newPowerup.GetComponent<SpriteRenderer>().color = Color.green;
                }
            }
            
            Destroy(this.gameObject);
        }
    }
    private void OnMouseDown()
    {
        print(this.gameObject.name);
        Destroy(this.gameObject);
    }
}
