using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalaMovement : MonoBehaviour
{
    public GameObject bola;
    public StartBola bolaEvent;
    int vida = 3;
    // Start is called before the first frame update
    void Start()
    {
        bolaEvent.onPerderVida += perdervida;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity =new Vector2(-10,0);
        } else if (Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(+10, 0);
        } else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.gameObject.GetComponent<SpriteRenderer>().color);
       if (collision.gameObject.GetComponent<SpriteRenderer>().color == Color.blue)
        {
            int nRand = Random.Range(2, 5);
            StartCoroutine(Espera(nRand));
        }
        Destroy(collision.gameObject);
    }



    void perdervida()
    {
        vida--;
        if (vida > 0)
        {
            this.gameObject.transform.position = new Vector3(0, -5, 0);
        } else
        {
            Destroy(this.gameObject);
        }

    }


    IEnumerator Espera(int segundos)
    {
        
        Debug.Log("entro");
        bola.GetComponent<Rigidbody2D>().simulated = false;
        yield return new WaitForSeconds(segundos);
        Debug.Log("salgo");
        bola.GetComponent<Rigidbody2D>().simulated = true;
    }
}
